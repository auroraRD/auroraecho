
package org.glassfish.samples.websocket;

import static Navixy.NavixyAPICalls.navixyGetTrackers;
import static Navixy.NavixyAPICalls.navixyTrackerGetState;
import static Navixy.NavixyAPICalls.getNavixyTrackerState;
import static Navixy.NavixyAPICalls.navixyElectronicLockOpenCommand2;
import static Navixy.NavixyAPICalls.printHashMap;

import Navixy.NavixyTrackerState;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class EchoDGA {

    /**
     * Incoming message is represented as parameter and return value is going to
     * be send back to peer.
     * </p> {@link javax.websocket.Session} can be put as a parameter and then
     * you can gain better control of whole communication, like sending more
     * than one message, process path parameters,
     * {@link javax.websocket.Extension Extensions} and sub-protocols and much
     * more.
     *
     * @param message incoming text message.
     * @return outgoing message.
     *
     * @see OnMessage
     * @see javax.websocket.Session
     */
    
    
    private Session session;

    @OnOpen
    public void onOpen(Session session){
        this.session = session;
    }
    
    
    @OnMessage
    public String echo(String message) throws IOException {
        Gson gson = new Gson();
        Boolean isSuccess;
        List<String> params = Arrays.asList(message.split(","));  // Recibimos 3 parametros separados por coma, y aqui los dividimos
        
        String userHash     = params.get(0);
        String trackerID    = params.get(1);
        String method       = params.get(2);
        
        Map<String, Object> navixyResponseMap = null;

        try {
            navixyResponseMap = navixyGetTrackers(userHash);
            
            isSuccess = (Boolean) navixyResponseMap.get("success");
            
            if (navixyResponseMap.containsKey("success") & ! isSuccess ) {
                
                System.out.println("ERROR EN HASH"); 
                printHashMap(navixyResponseMap);
                
                message = gson.toJson(navixyResponseMap);
                
                return message;
            }
        } catch (IOException ex) {
            Logger.getLogger(EchoDGA.class.getName()).log(Level.SEVERE, null, ex);
        }

        //ArrayList<NavixyTracker> navixyTrackers = listNavixyTrackers(navixyResponseMap);
        //NavixyTracker navixyTracker = NavixyAPICalls.getNavixyTrackerById(navixyTrackers, trackerID);

// SE INTENTA ABRIR EL CANDADO        
        
        try {
            Map<String, Object> responseOpenCommand = navixyElectronicLockOpenCommand2(userHash, trackerID, "888888"); //Abrir Candado

            isSuccess = (Boolean) responseOpenCommand.get("success");

            // Si la respuesta de la Funcion de Apertura de Candado retorna Error, devolvemos el Objeto de error como respuesta
            if (responseOpenCommand.containsKey("success") & ! isSuccess) {
                
                System.out.println("CANDADO APAGADO");
                printHashMap((Map<String, Object>) responseOpenCommand.get("status"));
                
                message = gson.toJson(responseOpenCommand);
                
                return message;
            }
        } catch (Exception ex) {
            Logger.getLogger(EchoDGA.class.getName()).log(Level.SEVERE, null, ex);
        }

        
// ##### PRIMER BUCLE - ESPERAR RESPUESTA DE CANDADO ABIERTO
// ##### SE ESPERA RECIBIR STATUS UNSEALED, O CONTADOR EN CERO, LO PRIMERO QUE OCURRA
        int conteo = 180;
        int counter = conteo;
        //message = "CERRADO";
        //boolean sealed = true;
        Map<String, Object> mapNavixyTrackerState;
        Object objTrackerState;
        int segundos =1;
        String stateValue;
        NavixyTrackerState navixyTrackerState;
        NavixyTrackerState.NavixyTrackerAdditional.Lock_State state;
        
        System.out.println("^^^^^^^^^^^^^^^^ Entro Bucle 1");

        
//----        
        do {
            // hay formas mas faciles de hacer esto, pero esta garantiza que los objetos POJO fueron creados a partir de los servicios de Navixy
            mapNavixyTrackerState = navixyTrackerGetState(userHash, trackerID);                         // obtengo el mapa de respuesta del servicio
            objTrackerState = mapNavixyTrackerState.get("state");                                       // obtengo el mapa con el state                                        
            navixyTrackerState = getNavixyTrackerState((Map<String, Object>)objTrackerState);           // lo convierto a objeto pojo
            state = navixyTrackerState.getAdditional().getLock_state();                                 //obtengo el objeto JSON -> additional en pojo
            stateValue = state.getValue();                                                              // obtengo el valor string del estado del candado
            
            message = gson.toJson(navixyTrackerState);
            
            if (stateValue.equals("sealed")) {                                       // si esta cerrado, damos otra iteracion
                counter--;
                System.out.println("BUCLE 1 ABRIENDO ("+ counter +"): Estado: " + stateValue + " " + state.getUpdated() + ".. Espero 1 Seg..");
                try {
                    Thread.sleep(segundos*1000); // Wait 1 seg.
                } catch (InterruptedException ex) {
                    Logger.getLogger(EchoDGA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }          
        } while (  counter > 0 & stateValue.equals("sealed")  ); // fin ciclo do while 

        System.out.println("BUCLE 1 FINALIZADO - STATE: " + stateValue + " NUMERO DE ITERACIONES: " + (conteo - counter));
//----        
        
        
// salida por WebSocket        
        if(method.equals("STAGE1") & counter > 0 & stateValue.equals("unsealed")) {
            // En caso de Stage1, la ejecucion continuara, pero debemos informar del cambio de estado al Websocket
            session.getBasicRemote().sendText(gson.toJson(navixyTrackerState));
        }

        
// ##### SEGUNDO BUCLE - ESPERAR RESPUESTA DE CANDADO CERRADO
// ##### SE ESPERA RECIBIR STATUS SEALED, O CONTADOR EN CERO, LO PRIMERO QUE OCURRA
            

//----
        System.out.println("^^^^^^^^^^^^^^^^ Entro Bucle 2");
        while(method.equals("STAGE1") & counter > 0 & stateValue.equals("unsealed")) {                           
            // si es unsealed es porque salio bien del ciclo anterior, sino salio por count cero
            mapNavixyTrackerState = navixyTrackerGetState(userHash, trackerID);
            objTrackerState = mapNavixyTrackerState.get("state");
            navixyTrackerState = getNavixyTrackerState((Map<String, Object>)objTrackerState);
            state = navixyTrackerState.getAdditional().getLock_state(); 
            stateValue = state.getValue();
            message = gson.toJson(navixyTrackerState);

            if (stateValue.equals("unsealed")) {
                counter--;
                System.out.println("BUCLE 2 CERRANDO ("+ counter +"): Estado: " + stateValue + " " + state.getUpdated() + ".. Espero 1 Seg..");
                try {
                    Thread.sleep(segundos*1000);                          // Pausa de 1 segundo
                } catch (InterruptedException ex) {
                    Logger.getLogger(EchoDGA.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        System.out.println(method + " FINALIZADO - STATE: " + stateValue + " NUMERO DE ITERACIONES: " + (conteo - counter));
        
//----    
        
        return message;
        
    }
}
